;;; use-package installation:
(require 'package)
(setq prelude-whitespace nil)
;;(setq package-enable-at-startup nil)
;;(add-to-list 'package-archives'("melpa" . "https://melpa.org/packages") t) ;; melpa listed 2x ?
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") 'APPEND)
(add-to-list 'package-archives'("melpa-stable" . "https://melpa-stable.milkbox.net/packages/") t)
;;(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(message "use-package Installed!")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; non use-package stuff
;;;;;;;;;;;;;;;;;;;;

(setq recenter-positions '(middle 0.01 0.99))
(setq scroll-margin 4)

;; fixing font size with linum-mode
(defvar original-font-size)
(defvar font-size)
(defvar original-font-name)

(setq original-font-size 14)
(setq font-size original-font-size)

(setq original-font-name
      ;;"JetBrains Mono"
      ;;"NovaMono"
      ;; "Fira Code"
      ;; "Fira Mono"
      "Menlo"
      ;; "Anonymous Pro"
      ;; "Source Code Pro"
      )

(defun change-font-size (f m)
  (setq font-size (funcall f font-size))
  (set-frame-font
   (format "%s-%d" original-font-name font-size) t t)
  (message (format "%s %d" m font-size)))

(defun increase-font-size ()
  (interactive)
  (change-font-size
   '(lambda (x) (+ 1 x))
   "Increasing font size to:"))

(defun decrease-font-size ()
  (interactive)
  (change-font-size
   '(lambda (x) (- x 1))
   "Decreasing font size to:"))

(defun reset-font-size ()
  (interactive)
  (change-font-size
   '(lambda (x) original-font-size)
   "Resetting font size to original value:"))

(global-set-key (kbd "C--") 'decrease-font-size)
(global-set-key (kbd "C-+") 'increase-font-size)
(global-set-key (kbd "C-x C-0") 'reset-font-size)
(reset-font-size)

(setq web-mode-markup-indent-offset 2)
(setq global-hl-line-mode 'f)

(setq whitespace-line-column 120)
(global-linum-mode 1)
(set-face-foreground 'linum "yellow")
(idle-highlight-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; USE-PACKAGE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq use-package-always-ensure t)

;; *****
(use-package doom-themes
  :ensure t
  :init (load-theme 'doom-moonlight))

;; *****
(use-package helm :ensure f)

;; **
(use-package display-line-numbers)

;; ****
(use-package wgrep)

;; *****
(use-package multiple-cursors
  :ensure t
  :init (progn (setq mc/list-file "~/.emacs.d/personal/.mc-lists.el"))
  :bind (("C->"     . mc/mark-next-like-this)
         ("C-<"     . mc/mark-previous-like-this)
         ("C-c C-<" . mc/mark-all-like-this)
         ("C-c C->" . mc-hide-unmatched-lines-mode)))

(use-package helm-swoop
  :bind (("C-s" . helm-swoop)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; clojure
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package flycheck-clj-kondo :ensure t)
(use-package cider-eval-sexp-fu :ensure t)
(use-package cljr-helm :ensure t)

(use-package cider
  :ensure nil ;; provided by prelude, so don't ensure here
  :bind (("C-c C-d C-b" . cider-debug-defun-at-point))
  :config
  (setq cider-repl-history-size 1000)
  (setq cider-repl-result-prefix "#_#_=> "))

(defconst personal/work-machine
  (string= system-name "bryans-MacBook-Pro-2.local"))

(use-package clojure-mode
  :ensure
  nil
  :bind
  ("C-c C-<return> a m" . lsp-clojure-add-missing-libspec)
  :init
  (add-to-list 'auto-mode-alist '("\\.joke\\'" . clojure-mode))
  (add-to-list 'auto-mode-alist '("\\.bb\\'" . clojure-mode))
  :config
  (defun my-clojure-mode-hook ()
    (require 'flycheck-clj-kondo)
    (require 'clj-refactor)
    (clj-refactor-mode 1)
    (yas-minor-mode 1) ; for adding require/use/import statements
    ;; This choice of keybinding leaves cider-macroexpand-1 unbound
    ;;(cljr-add-keybindings-with-prefix "C-c C-r")
    )

  (when personal/work-machine

    (defun ffx! ()
      (interactive)
      (insert "(do
 (require '[aclaimant.services.core.db.customer-schema.fns])
 (aclaimant.services.core.db.customer-schema.fns/transaction-in-schema 
   \"ffx\"
   ))")
      (backward-char 2))

    ;; Overwrite prelude indent -> don't indent the entire file
    (global-set-key (kbd "C-c n") 'crux-indent-defun)

    (setq cider-known-endpoints
          '(("acl-service" "local.aclaimant.com" "7000")
            ("acl-cljs-repl" "localhost" "7888")
            ("acl-jobs" "local.aclaimant.com" "7001")
            ("acl-alerter" "local.aclaimant.com" "7002")
            ("acl-twilio" "local.aclaimant.com" "7004")
            ("acl-paper-pusher" "local.aclaimant.com" "7005")
            ("acl-dashboard" "local.aclaimant.com" "7888"))))
  (add-hook 'clojure-mode-hook #'my-clojure-mode-hook))

(global-set-key (kbd "C-x x b") 'helm-bookmarks)

(use-package clj-refactor :ensure t)

(use-package lsp-mode
  :ensure t
  :hook ((clojure-mode . lsp)
         (clojurec-mode . lsp)
         (clojurescript-mode . lsp)
         (scala-mode . lsp)
         (lsp-mode . lsp-enable-which-key-integration))
  :config
  
  (setenv "PATH" (concat "/usr/local/bin" path-separator (getenv "PATH")))
  (setq lsp-prefer-flymake nil)
  (dolist (m '(clojure-mode
               clojurec-mode
               clojurescript-mode
               clojurex-mode))
    (add-to-list 'lsp-language-id-configuration `(,m . "clojure")))
  (setq lsp-clojure-server-command '("bash" "-c" "clojure-lsp") ;; Optional: In case `clojure-lsp` is not in your PATH
        lsp-enable-indentation nil
        lsp-file-watch-threshold 10000

        gc-cons-threshold (* 100 1024 1024)
        read-process-output-max (* 1024 1024)
        lsp-idle-delay 0.500
        lsp-log-io nil
        lsp-completion-provider :capf
        treemacs-space-between-root-nodes nil
        company-minimum-prefix-length 1
        lsp-lens-enable nil
        lsp-signature-auto-activate nil
        cljr-add-ns-to-blank-clj-files nil
        ;; lsp-enable-indentation nil ; uncomment to use cider indentation instead of lsp
        ;; lsp-enable-completion-at-point nil ; uncomment to use cider completion instead of lsp
        
        )
  :commands lsp)

(use-package lsp-treemacs)

(use-package lsp-metals
  :config (setq lsp-metals-treeview-show-when-views-received t))

(use-package yasnippet)

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

(use-package company-lsp :ensure t :commands company-lsp)
(use-package helm-lsp
  :ensure t
  :commands helm-lsp-workspace-symbol)

;; Use the Debug Adapter Protocol for running tests and debugging
(use-package posframe
  ;; Posframe is a pop-up tool that must be manually installed for dap-mode
  )
(use-package dap-mode
  :hook
  (lsp-mode . dap-mode)
  (lsp-mode . dap-ui-mode))

(use-package avy :ensure f
  :bind (("C-'" . avy-goto-line)
         ("M-g w" . avy-goto-word-1)
         ("M-g c" . avy-goto-char)))

(defun cljfmt ()
  (interactive)
  (when (or (eq major-mode 'clojure-mode)
            (eq major-mode 'clojurescript-mode))
    (shell-command-to-string (format "cljfmt %s" buffer-file-name))
    (revert-buffer :ignore-auto :noconfirm)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END CLOJURE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package dumb-jump :ensure t)
(use-package git-link :ensure t)

(use-package prism :ensure t
  :config
  (prism-set-colors :num 16
    :desaturations (cl-loop for i from 0 below 16 collect (* i 2.5))
    ;; :lightens (cl-loop for i from 0 below 16
    ;;                    collect (* i 0.5))
    :colors (list "red" "orange" "yellow" "green" "cyan" "violet" "magenta")
    :comments-fn
    (lambda (color)
      (prism-blend color
                   (face-attribute 'font-lock-comment-face :foreground) 0.25))
    :strings-fn
    (lambda (color)
      (prism-blend color "white" 0.5))))

(use-package highlight
  :ensure t
  :config
  (global-hl-line-mode -1)
  (global-set-key (kbd "C-<return>") 'hlt-highlight-symbol)
  (global-set-key (kbd "M-<return>") 'hlt-unhighlight-symbol))

(use-package git-gutter+ :ensure t)
(use-package dumb-jump :ensure t)
(use-package git-link :ensure t)
;;(use-package hlinum :ensure t)
(use-package linum-relative :ensure t)
;;(use-package ob-clojure :ensure t)

;; Enable scala-mode for highlighting, indentation and motion commands
(use-package scala-mode
  :interpreter
  ("scala" . scala-mode))

;; Enable sbt mode for executing sbt commands
(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
  ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
  (setq sbt:program-options '("-Dsbt.supershell=false"))
  )

(use-package flycheck
  :init (global-flycheck-mode))

;; Add metals backend for lsp-mode
(use-package lsp-metals
  :config (setq lsp-metals-treeview-show-when-views-received t))

(use-package ac-emoji
  :config (ac-emoji-setup))

(use-package golden-ratio
  :config (golden-ratio-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END USE-PACKAGE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(sql-set-product "postgres")

(sql-set-product-feature 'postgres :prompt-regexp "^[_[:alnum:]\\-:]*=\\*?[#>] *")
(sql-set-product-feature 'postgres :prompt-cont-regexp nil
                         ;;"^[_[:alnum:]\\-:]*[-(]\\*?[#>] *"
                         )

;; (global-flycheck-mode -1)
;; (remove-hook 'prog-mode 'flycheck-mode)

;; ======================================================================
;; ============================== org-mode ==============================
;; ======================================================================

(defun my-org-mode-prefs ()
  (progn
    (require 'ob-clojure)
    (flycheck-mode -1)
    (visual-line-mode 1)))

  ;; for capture:
(setq org-directory "~/dv/notes")
(setq org-default-notes-file (concat org-directory "/todo.org"))
(setq org-agenda-files (list org-directory))
(add-hook 'org-mode-hook 'my-org-mode-prefs)


(require 'org-tempo) ;; <s,TAB  support

(defun org-insert-source-block (language)
  "Asks name, language, switches, header.
Inserts org-mode source code snippet"
  (interactive "slanguage?")
  (insert (format "\n#+BEGIN_SRC %s\n\n#+END_SRC" language))
  (forward-line -1)
  (goto-char (line-end-position)))

;; ======================================================================
;; ========================== end org-mode ==============================
;; ======================================================================

;;(setq vc-handled-backends nil)

(defun toggle-frame-split ()
  "If the frame is split vertically, split it horizontally or vice versa.
Assumes that the frame is only split into two."
  (interactive)
  (unless (= (length (window-list)) 2) (error "Can only toggle a frame split in two"))
  (let ((split-vertically-p (window-combined-p)))
    (delete-window) ; closes current window
    (if split-vertically-p
        (split-window-horizontally)
      (split-window-vertically)) ; gives us a split with the other window twice
    (switch-to-buffer nil))) ; restore the original window in this part of the frame

(global-set-key (kbd "C-x C-|") 'toggle-frame-split)

(defun setup-m-and-s ()
  (progn (setq mac-command-modifier 'super)
         (setq mac-option-modifier 'meta)
         (message "Command is now bound to SUPER and Option is bound to META.")))

(setup-m-and-s)

(provide 'personal)
;;; personal.el ends here

