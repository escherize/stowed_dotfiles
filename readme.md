# Usage:

    git clone git@gitlab.com:escherize/stowed_dotfiles.git
    cd stowed_dotfiles
    # brew install stow
    stow -R -t ~ emacs -v


# to install all:

    install_all.sh
