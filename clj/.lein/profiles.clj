{:user {:plugins [[lein-cljfmt "0.5.7"]
                  [lein-bikeshed "0.5.1"]
                  [jonase/eastwood "0.2.5"]
                  [lein-auto "0.1.3"]
                  [lein-ancient "0.6.15"]
                  [lein-count "1.0.8"]]}}
